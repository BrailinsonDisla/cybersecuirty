#!/usr/bin/env python3

#########################################################################
#  Script     : scan.nmap.py
#  Author     : BrailinsonDisla
#  Date       : July 30th, 2018
#  Last Edited: July 30th, 2018 @ ~ 16:00 , BrailinsonDisla
#########################################################################
# Purpose:
#    --
#
# Requirements:
#    Python 3 - Nmap
#		>> apt-get install python3-nmap
#
# Method:
#    If arguments are given:
#
# Syntax:
#    # 1. scan.nmap.py
#    # 2. scan.nmap.py
#    # 3. scan.nmap.py
#    # 4. scan.nmap.py
#
# Notes:None.
#########################################################################
# Import the nmap module.
import nmap

# Create an instance for the port scanner.
scanner = nmap.PortScanner()

# Store the version of nmap.
version = scanner.nmap_version()

# Inform the user of version of nmap being used.
print ('Running Nmap Version ' + str(version[0])+'.'+str(version[1]) + '\n')

#########################################################################
# HOSTS INFORMATION
#########################################################################
# Set the default host to localhost.
hosts = '127.0.0.1'

# Ask the user for private IP Address class.
print ('Please enter private IP Address class [A/B/C]: ', end='')

# Read user response.
prompt = input().lower()

# Check if user provided an invalid response, if so ask again.
while prompt not in ['A', 'a', 'B', 'b', 'C', 'c']:
	# Ask the user again.
	print ('Please enter A, B or C / a, b or c: ', end='')

	# Read response.
	prompt = input().lower()

# Set hosts range.
if prompt == 'a':
	# Set hosts to private IP class A range.
	hosts = '10.0-255.0-255.0-255'

elif prompt == 'b':
	# Set hosts to private IP class B range.
	hosts = '172.16-31.0-255.0-255'

else:
	# Set hosts to private IP class C range.
	hosts = '192.168.0-255.0-255'

print ('\tThe hosts to scan are: ' + hosts + '\n')

#########################################################################
# PORTS INFORMATION
#########################################################################
# Set the default ports to all.
ports = '0-65535'

# Set the default temporary ports to none.
temp_ports = []

# Ask the user if specifying ports to scan as a list or range.
print ('Specifying ports to scan as list or range [L/R] : ', end='')

# Read user response.
prompt = input().lower()

# Check if user provided an invalid response, if so ask again.
while prompt not in ['l', 'r', 'list', 'range']:
	# Ask the user again.
	print ('Please answer L, R, List or Range: ', end='')

	# Read user response.
	prompt = input().lower()

#########################################################################
# PORTS INFORMATION - LIST
#########################################################################
# Set ports list.
if prompt in ['l', 'list']:
	# Expect invalid input to ask again if necessary.
	invalid = True

	# Check if list provided is valid.
	while invalid:
		# Set invalid to False.
		invalid = False

		# Ask the user to specify a list of ports to scan.
		print ('\tSpecify a valid list of ports to scan: ', end='')

		# Read user response.
		prompt = input().lower()

		# Split list of ports by space and commas.
		prompt = prompt.replace(', ', ' ')
		prompt = prompt.replace(',', ' ')
		prompt = prompt.replace('; ', ' ')
		prompt = prompt.replace(';', ' ')
		prompt = prompt.split(' ')

		# Check if all ports are valid.
		for port in prompt:
			# Check all the ports in the list are integers.
			try: 
				# Parse the port string to an integer.
				temp_port = int(port.replace(' ', ''))

				# Check if port is already in the list.
				if temp_port not in temp_ports:
					# Check if port is in range.
					if temp_port >= 0 and temp_port <= 65535:
						# Add port to temp ports.
						temp_ports += [port]

			except:
				# Set invalid to true.
				invalid = True

		# Set if ports provided were all valid.
		if not invalid:
			# Set ports to temp_ports.
			ports = ','.join(temp_ports)

			print (ports)

#########################################################################
# PORTS INFORMATION - RANGE
#########################################################################
# Set ports range.
if prompt in ['r', 'range']:
	# Expect invalid input to ask again if necessary.
	invalid = True

	# Check if range provided is valid.
	while invalid:
		# Set invalid to false.
		invalid = False

		# Ask the user to specify a range of ports to scan.
		print ('Specify a valid range of ports to scan: ', end='')

		# Read user response.
		prompt = input().lower()

		# Split range of ports by hypen.
		prompt = prompt.replace(' - ', '-')
		prompt = prompt.replace('- ', '-')
		prompt = prompt.replace(' -', '-')
		prompt = prompt.split('-')

		# Check if it is a range.
		if len(prompt) == 2:
			# Check if ports range is valid.
			try: 
				# Parse the lower port string to an integer.
				temp_low_port = int(prompt[0])

				# Parse the higher port string to an integer.
				temp_high_port = int(prompt[1])

				# Check if port range is valid.
				if temp_low_port >= temp_high_port:
					# Set helper to true.
					invalid = True
				# Check ports are in ports range.
				elif temp_low_port < 0 or temp_high_port > 65535:
					# Set helper to true.
					invalid = True
				else:
					# Set ports range.
					ports = str(temp_low_port) + '-' + str(temp_high_port)

			except:
				# Set helper to false.
				invalid = True

		else:
			# Set invalid to true.
			invalid = True

#########################################################################
# SCAN TYPE INFORMATION
#########################################################################
# Format printout.
print ('\n----------------------- SCAN TYPES -----------------------')

# Print scan type to number map and how they work.
print ('1 - TCP Scan')
print ('\t\t   SYN ------>        ')
print ('\t\t       <------ SYN/ACK')
print ('\t\t   ACK ----->       \n')


print ('2 - SYN Scan')
print ('\t\t   SYN ------>        ')
print ('\t\t       <------ SYN/ACK')
print ('\t\t   RST ----->       \n')

print ('3 - Null Scan')
print ('\t\t   FIN + PSH + URG ------>             ')
print ('\t\t                   <------ RST - Closed')
print ('\t\t                   <------ [X] - Open\n')

print ('4 - XMas Scan')
print ('\t\t   _EMPTY_ ------>             ')
print ('\t\t           <------ RST - Closed ')
print ('\t\t           <------ [X] - Open')
print ('----------------------------------------------------------')

# Ask the user to specify scan type.
print ('\nSpecify scan type: ', end='')

# Read user response.
prompt = input().lower()

# Check if user provided an invalid response, if so ask again.
while prompt not in ['1', '2', '3', '4']:
	# Ask the user again.
	print ('Please choose scan type [1/2/3/4]: ', end='')

	# Read user response.
	prompt = input().lower()

# Set scan type according to number mapping.
scan_type = '-sN'

# Set arguments for scan type.
if prompt == '1':
	scan_type = '-sT'
elif prompt == '2':
	scan_type = '-sS'
elif prompt == '3':
	scan_type = '-sN'
else:
	scan_type = '-sX'

#########################################################################
# SCAN
#########################################################################
# Scan.
scanner.scan(hosts, ports)

# Print command line version of scan.
print ('\nCommand Line Scan: ' + str(scanner.command_line()))

#########################################################################
# SCAN - STATS
#########################################################################
# Print host's status
for host in scanner.all_hosts():
	# Print host IP.
	print ('\nHost IP 		:	' + host)
	print ('Host Status		:	' + scanner[host].state())

	# Print protocols and open ports.
	for protocol in scanner[host].all_protocols():
		# Print Protocol.
		print ('    [' + protocol.upper() + ']    ')

		# Print ports and state.
		port_list = scanner[host][protocol].keys()

		for port in port_list:
			print ('      Port: ' + str(port) + '	' + scanner[host][protocol][port]['state'])
